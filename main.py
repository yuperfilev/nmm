import numpy as np
import gaus_seidel_lent
import matplotlib.pyplot as plt

def psi(i, x, xk, hk):
    ksi = (x-xk)/hk
    if i == 0:
        return 1 - 3*pow(ksi, 2) + 2*pow(ksi, 3)
    if i == 1:
        return hk*(ksi - 2*pow(ksi, 2) + pow(ksi, 3))
    if i == 2:
        return 3*pow(ksi, 2) - 2*pow(ksi, 3)
    if i == 3:
        return hk*(-pow(ksi, 2) + pow(ksi, 3))


def local(X, F, w, xk, xkp1, last):
    A = np.zeros((4, 4))
    G = np.zeros((4, 4))
    b = np.zeros(4)
    points = []
    hk = xkp1-xk
    G[0, 0] = G[2, 2] = 36
    G[0, 2] = G[2, 0] = -36
    G[0, 1] = G[1, 0] = G[0, 3] = G[3, 0] = 3*hk
    G[1, 2] = G[2, 1] = G[2, 3] = G[3, 2] = -3*hk
    G[1, 1] = G[3, 3] = 4*hk**2
    G[1, 3] = G[3, 1] = -1*hk**2
    for i in range(len(X)):
        if X[i] < xkp1:
            if X[i] >= xk:
                points.append([i, X[i]])
        else:
            if last == True and X[i] == xkp1:
                points.append([i, X[i]])
            break
    for point in points:
        for i in range(4):
            for j in range(4):
                A[i, j] += w[point[0]] * psi(i, point[1], xk, hk) * psi(j, point[1], xk, hk)
            b[i] += w[point[0]] * psi(i, point[1], xk, hk) * F[point[0]]
    return A, G/(30*hk), b


def build(X, F, Xsp, w, alpha):
    n = len(Xsp)
    globalA = np.zeros((2*n, 7))
    globalb = np.zeros(2*n)
    for k in range(n-2):
        ind = k*2
        localA, localG, localb = local(X, F, w, Xsp[k], Xsp[k+1], False)
        for i in range(4):
            for j in range(4):
                globalA[ind + i,j-i+3] += localA[i,j] + alpha*localG[i,j]
        globalb[ind:ind+4] += localb
    ind = 2*(n-2)
    i = j = ind
    localA, localG, localb = local(X, F, w, Xsp[-2], Xsp[-1], True)
    for i in range(4):
        for j in range(4):
            globalA[ind + i,j-i+3] += localA[i,j] + alpha*localG[i,j]
    globalb[ind:ind+4] += localb
    return globalA, globalb

def data_prep():
    X = np.linspace(-17, 25, 60)
    Y = 7*np.sin(X) + np.random.normal(0, 3, len(X))
    Y[15] = 40
    Y[7] = -73
    Y[17] = -18
    Y[19] = 40
    Y[27] = -100
    return X, Y

def P(x, Xsp, q):
    k = 0
    if x == Xsp[-1]:
        k = len(Xsp)-2
    else:
        for i in range(len(Xsp)-1):
            if x >= Xsp[i] and x < Xsp[i+1]:
                k = i
                break
    hk = Xsp[k+1] - Xsp[k]
    return q[2*k] * psi(0, x, Xsp[k], hk) + q[2*k+1] * psi(1, x, Xsp[k], hk)\
        + q[2*k+2] * psi(2, x, Xsp[k], hk) + q[2*k+3] * psi(3, x, Xsp[k], hk)


def show(X, Y, F):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    line1, = ax1.plot(X, Y, 'g', label='F')
    ax1.set_xlabel('x')
    ax1.set_ylabel('F', color='g')
    ax2 = ax1.twinx()
    line2, = ax1.plot(X, F, 'r', label='P')
    ax2.set_ylabel('P', color='r')
    plt.show()


def main():
    X, Y = data_prep()
    Xsp = np.linspace(-17, 25, 5)
    print(Xsp)
    n = len(X)
    w = np.ones((n,))
    alpha = 0.1
    eps = 1e-5
    while True:
        A, b = build(X, Y, Xsp, w, alpha)
        q = gaus_seidel_lent.gaus_seidel(A,b)
        F = np.zeros(n)
        for i in range(n):
            F[i] = P(X[i], Xsp, q)
        f_mean = np.mean(abs(F-Y))
        k = 0
        for i in range(n):
            if abs(F[i]-Y[i]) > 0.5*f_mean and w[i] > eps:
                w[i] /= abs(F[i]-Y[i])
                k += 1
        print(w)
        print('\n')
        # print(F-Y)
        if k == 0:
            show(X, Y, F)
            break
        

if __name__ == "__main__":
    main()
