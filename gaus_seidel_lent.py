import numpy as np

def gaus_seidel(A, b, x0 = None, eps = 1e-5, max_iteration = 10000):
    n = len(b)
    x0 = [0] * n if x0 == None else x0
    x1 = x0[:]
    beta = len(A[0])
    w = 0.5
    m = int((beta - 1)/2)
    for __ in range(max_iteration):
        l = 0
        for i in range(n):
            s = 0
            if i > m:
                l+=1
            for j in range(beta):
                k = j + l
                if abs(i-k) <= m and k < n:
                    #print(str(i) + ' ' + str(k) + ' ' + str(l))
                    s += -A[i,k-i+m] * x1[k]
            x1[i] = x0[i] + w*(b[i] + s)/A[i,m]
        if all(abs(x1[i]-x0[i]) < eps for i in range(n)):
            return x1
        x0 = x1[:]
    raise ValueError('Solution does not converge')